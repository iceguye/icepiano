#!/usr/bin/python3

#    Copyright © 2020 - 2021 IceGuye

#     This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, version 3 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import sys
import pygame.display
import pygame.midi
import pygame.mixer
import threading
import time
import math
from random import random
from os import listdir
from os.path import isfile
from playinglist import start_fadeout_set_callback, stoppers_down, get_channel_busy_set_callback, shrinking_playing_idx, keypress_playing_idx
from preparesound import get_layer
import pickle
import atexit

sustain_pedal = 0
playing = [None] * 128
keypress = [0] * 128
keypress2 = []
express = 127
volume = 0
mid_pedal = 0
mid_pedal_hold = [0] * 128
mid_pedal_hold2 = []
dbfs = -999
max_dbfs_all = -65532
playing_idx_cnt = 0
playing_idx = []
recording = False
record_data = []
advanced = False

# Select your midi device
pygame.midi.init()
def print_devices():
    for n in range(pygame.midi.get_count()):
        print (n,pygame.midi.get_device_info(n))

print_devices()
selected_device = input("Please select a MIDI device: ")
selected_device = int(selected_device)
piano_input = pygame.midi.Input(selected_device)

chan_inp = ""
channel = 0
        
pygame.display.init()
pygame.display.set_caption('IcePiano')
pygame.mixer.pre_init(48000, 32, 2, 1024)
pygame.mixer.init()
pygame.mixer.set_num_channels(256)

# Import notes from the generated sounds.
notes = []
wc = 0
while (wc <=127):
    notes.append([])
    wc = wc + 1
    
notes_no_sound = []
wc = 0
while (wc <=127):
    notes_no_sound.append([])
    wc = wc + 1
    
fc = 0
for i in listdir("installed-samples"):
    if i != ".keep" and "perfect" not in i and "origin" not in i:
        filepath = "installed-samples/" + i
        note = int(i[0:3])
        db_start = 3
        db_end = i.find(".wav")
        dbfs = float(i[db_start:db_end])
        notes[note].append([pygame.mixer.Sound(filepath), dbfs, i])
        notes_no_sound[note].append([None, dbfs, i])
        completed = fc / len(listdir("installed-samples")) * 100
        print("Building pygame Sound objects: " + i + " ... "+ str(int(completed)) + "%")
    fc = fc + 1
    
# GUI building

window = pygame.display.set_mode((1920, 420))
keyboard_base_img = pygame.image.load("./images/keyboard-88.png")
#keyboard_base_img = keyboard_base_img.convert()
window.blit(keyboard_base_img, (0, 0))
keyimgs = [None] * 128
keyimg_surfaces = [None] * 128
wc = 0
while (wc < 128):
    print("Building GUI... " + str(int(wc/128*100)) + "%" )
    keyid = str(wc)
    if isfile("./images/key-" + keyid + ".png"):
        keyimgs[wc] = pygame.image.load("./images/key-" + keyid + ".png")
        #keyimgs[wc] = keyimgs[wc].convert()
        keyimgs[wc].set_alpha(0)
        window.blit(keyimgs[wc], (0, 0))
    wc = wc + 1
sus_pd_img = pygame.image.load("./images/sus-pd.png")
sus_pd_img.set_alpha(0)
soft_pd_img = pygame.image.load("./images/soft-pd.png")
soft_pd_img.set_alpha(0)
sos_pd_img = pygame.image.load("./images/sos-pd.png")
sos_pd_img.set_alpha(0)
window.blit(sus_pd_img, (0, 0))
pygame.display.flip()
    
print("Completed. Enjoy!")

# Play the sound. When playing the sound, the sound's volume will be
# convert back to the system volume as decibel.

def exit_handler():
    if recording:
        time_str = str(int(time.time()))
        record_file = open("midirecords/record_data-" + time_str + ".pkl", "wb")
        pickle.dump(record_data, record_file, pickle.HIGHEST_PROTOCOL)
        record_file.close()
        
atexit.register(exit_handler)

def play_sound(note, express, volume):
    global playing_idx_cnt
    global playing_idx
    if volume == 0 or express == 0:
        dbfs = -999
    else:
        dbfs = 40 * math.log((volume*express)/(127*127))
    set_volume = math.exp(dbfs/8.65618)    
    final_layer = get_layer(note, express, volume, notes_no_sound)
    final_sound = notes[note][final_layer][0]
    final_sound.set_volume(set_volume)
    pygame.mixer.Channel(playing_idx_cnt).set_volume(1.0)
    pygame.mixer.Channel(playing_idx_cnt).play(final_sound)
    playing_idx.insert(0, [playing_idx_cnt, note])
    if len(playing_idx) > 255:
        playing_idx.pop(len(playing_idx)-1)
    if playing_idx_cnt < 255:
        playing_idx_cnt = playing_idx_cnt + 1
    else:
        playing_idx_cnt = 0

def fadeout(channel, key):
    fotime=random() * (0.3 - 0.1) + 0.1
    seg = 0.05
    svl = 1.0
    min_vl = 0 * seg
    while (svl > min_vl):
        svl = svl - seg
        pygame.mixer.Channel(channel).set_volume(svl)
        time.sleep(fotime / ((1 - min_vl) / seg))
    #if svl <= min_vl:
        #pygame.mixer.Channel(channel).stop()
            
def start_fadeout(item):
    threading.Thread(target=fadeout, args=item).start()

def get_channel_busy(channel):
    is_busy =  pygame.mixer.Channel(channel).get_busy()
    return is_busy

start_fadeout_set_callback(start_fadeout)
get_channel_busy_set_callback(get_channel_busy)

# Main loop:      
while True:

    for evt in pygame.event.get():
        if evt.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif evt.type == pygame.KEYUP:
            if evt.key == pygame.K_a:
                mods = pygame.key.get_mods()
                if mods & pygame.KMOD_CTRL and mods & pygame.KMOD_ALT:
                    advanced = True
                    print("Advanced Mode")
        elif evt.type == pygame.KEYDOWN and advanced:
            if evt.key != pygame.K_RETURN:
                chan_inp += pygame.key.name(evt.key)
                print(chan_inp)
            else:
                channel = int(chan_inp) - 1
                print("Channel selected: " + str(channel+1))
                chan_inp = ""
                advanced = False

    playing_idx = shrinking_playing_idx(playing_idx)

    if piano_input.poll():
        # Sound control
        midi_read_all =  piano_input.read(1)
        midi_read = midi_read_all[0][0]
        print(midi_read)
        if recording:
            record_data.extend(midi_read_all)
            
        if midi_read[0] == 176 + channel and midi_read[1] == 7:
            express = midi_read[2]
        if midi_read[0] == 176 + channel and midi_read[1] == 64:
            sustain_pedal = midi_read[2]
        if midi_read[0] == 176 + channel and midi_read[1] == 1:
            mid_pedal = midi_read[2]
            if mid_pedal != 0:
                mid_pedal_hold2 = keypress2
                for i in mid_pedal_hold2:
                    mid_pedal_hold[i[0]] = i[1]
            else:
                mid_pedal_hold = [0] * 128
                mid_pedal_hold2 = []
        if midi_read[0] == 144 + channel and midi_read[2] == 0:
            keypress[midi_read[1]] = midi_read[2]
            for item in list(keypress2):
                if item[0] == midi_read[1]:
                    keypress2.remove(item)
        if midi_read[0] == 144 + channel and midi_read[2] != 0:
            volume = midi_read[2]
            keypress[midi_read[1]] = midi_read[2]
            keypress2.append([midi_read[1], midi_read[2]])
            play_sound(midi_read[1], express, volume)
        elif sustain_pedal == 0:
            threading.Thread(target=stoppers_down, args=[playing_idx, keypress, mid_pedal_hold]).start()
            playing_idx = keypress_playing_idx(playing_idx, keypress, mid_pedal_hold)
            
        # Graphic control
        window.blit(keyboard_base_img, (0, 0))
        for item in keypress2:
            keyimgs[item[0]].set_alpha(item[1]*1.6)
            window.blit(keyimgs[item[0]], (0, 0))
        sus_pd_img.set_alpha(sustain_pedal*1.6)
        window.blit(sus_pd_img, (0, 0))
        soft_pd_img.set_alpha((127-express)*1.6)
        window.blit(soft_pd_img, (0, 0))
        sos_pd_img.set_alpha(mid_pedal*1.6)
        window.blit(sos_pd_img, (0, 0))
        pygame.display.flip()
