#!/usr/bin/python3

#    Copyright © 2017-2020 IceGuye.
#
#    This program is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as
#    published by the Free Software Foundation, version 3 of the
#    License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

from distutils.core import setup, Extension

setup(name="IcePiano High Performance",
      version="0.1",
      description="Python extensions in C for improving the performance of IcePiano.",
      author="IceGuye", author_email="ice.guye@bk.ru",
      ext_modules=[Extension("playinglist", ["playinglist.c"]),
                   Extension("preparesound", ["preparesound.c"])])
