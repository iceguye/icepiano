#!/usr/bin/python3

import os

for filename in os.listdir("installed-samples/"):
    os.remove("installed-samples/" + filename)
keep = open("installed-samples/.keep", "w")
keep.close()
    
for filename in os.listdir("midirecords/"):
    os.remove("midirecords/" + filename)
keep = open("midirecords/.keep", "w")
keep.close()
