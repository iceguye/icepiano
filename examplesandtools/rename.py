#    Copyright © 2020 IceGuye,

#     This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, version 3 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import os

for filename in os.listdir("samples"):
    vpos = filename.find("v")
    note = filename[:vpos]
    a0 = 21
    start_point = 0
    if "A" in note:
        start_point = a0
    elif "A#" in note or "Bb" in note:
        start_point = a0 + 1
    elif "B" in note:
        start_point = a0 + 2
    elif "G#" in note or "Ab" in note:
        start_point = a0 - 1
    elif "G" in note:
        start_point = a0 - 2
    elif "F#" in note or "Ab" in note:
        start_point = a0 - 3
    elif "F" in note:
        start_point = a0 - 4
    elif "E" in note:
        start_point = a0 - 5
    elif "D#" in note or "Eb" in note:
        start_point = a0 - 6
    elif "D" in note:
        start_point = a0 - 7
    elif "C#" in note or "Db" in note:
        start_point = a0 - 8
    elif "C" in note:
        start_point = a0 - 9
    octave = int(note[len(note)-1:])
    midi_num = start_point + (octave * 12)
    str_midi_num = str(midi_num)
    if len(str_midi_num) == 1:
        str_midi_num = "00" + str_midi_num
    if len(str_midi_num) == 2:
        str_midi_num = "0" + str_midi_num
        
    new_filename = str_midi_num + "-" + filename
    
    src = "samples/" + filename
    dst = "samples/" + new_filename
    print(src + " ====> " + dst)
    os.rename(src, dst)
