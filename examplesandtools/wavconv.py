#    Copyright © 2020 IceGuye,

#     This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, version 3 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import os

for filename in os.listdir("samples"):
    filepath = "samples/"+filename
    if "-soxr.wav" in filepath:
        os.system("rm -rf " + filepath)
    else:
        dot_pos = filepath.rfind(".")
        new_filepath = filepath[0:dot_pos]
        new_filepath = new_filepath + "-soxr.wav"
        sox_cmd = "sox " + filepath + " -b 16 -r 44100 " + new_filepath
        os.system(sox_cmd)
        os.system("rm -rf " + filepath)
