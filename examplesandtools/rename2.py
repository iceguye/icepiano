#    Copyright © 2020 IceGuye,

#     This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, version 3 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import os

for filename in os.listdir("samples"):
    vpos = filename.find("-")
    note = filename[:vpos]
    note = int(note)
    note = note + 20
    str_midi_num = str(note)
    if len(str_midi_num) == 1:
        str_midi_num = "00" + str_midi_num
    if len(str_midi_num) == 2:
        str_midi_num = "0" + str_midi_num
        
    new_filename = str_midi_num + "-" + filename
    
    src = "samples/" + filename
    dst = "samples/" + new_filename
    print(src + " ====> " + dst)
    os.rename(src, dst)
