#!/usr/bin/python3

#    Copyright © 2020 - 2021 IceGuye

#     This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, version 3 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.
import math
import sys
from os import listdir, remove, system, rename
from os.path import isfile, join
from shutil import copy
from pydub import AudioSegment

arg_ls = sys.argv
if (len(arg_ls) > 2):
    error = "You can only have one argument."
    print(error)
    sys.exit()

for i in listdir("installed-samples"):
    remove("installed-samples/"+i)

for i in listdir("perfecths-samples"):
    new_file_name = i.replace("perfect-", "")
    if "keep" not in i:
        copy("perfecths-samples/"+i, "installed-samples/"+new_file_name)
    
# Change is A hertz based on the user's option

if (len(arg_ls) == 2):
    ahz = float(arg_ls[1])
    chhz = ahz / 440.0
    chsemi = 12 * math.log(chhz) / math.log(2)
    chcent = chsemi * 100
    speed = 1 + (2 ** (chsemi / 12) - 1)
    for filename in listdir("installed-samples"):
        tmp_name = "tmp-" + filename
        filepath = "installed-samples/" + filename
        tmp_path = "installed-samples/" + tmp_name
        rename(filepath, tmp_path)
        sox_cmd = "sox " + tmp_path  + " " + filepath + " speed " + str(speed) + " rate -v"
        system(sox_cmd)
        remove(tmp_path)
        print("Changing pitch: " + filepath)

# Calculating the related dbfs and amplitutde data.
max_dbfs_all = -9999
max_dbfs_list = []
fc = 0
for i in listdir("installed-samples"):
    filepath = "installed-samples/" + i
    sound = AudioSegment.from_file(filepath, format="wav")
    max_dbfs = sound.max_dBFS
    max_dbfs_list.append([i, max_dbfs])
    if max_dbfs > max_dbfs_all:
        max_dbfs_all = max_dbfs
    print("Calculating the max dbfs: " + str(i) + "... " + str(int(fc/len(listdir("installed-samples"))*100)) + "%")
    fc = fc + 1

for i in max_dbfs_list:
    i[1] = i[1] - max_dbfs_all
    filename = i[0]
    filepath = "installed-samples/" + filename
    note = filename[0:3]
    new_filename = note + str(i[1]) + ".wav"
    new_filepath = "installed-samples/" + new_filename
    rename(filepath, new_filepath)
    
# Interpolating missing notes
first_filled = 999
last_filled = -999
filled_notes = []
empty_notes = []
for i in listdir("installed-samples"):
    note = int(i[0:3])
    if note not in filled_notes:
        filled_notes.append(note)
for i in filled_notes:
    if last_filled < i:
        last_filled = i
    if first_filled > i:
        first_filled = i
wc = first_filled
while (wc <= last_filled):
    if wc not in filled_notes:
        empty_notes.append(wc)
    wc = wc + 1
fc = 0
for i in empty_notes:
    min_gap = 9999
    semitone = 9999
    closest_note = 9999
    for j in filled_notes:
        gap = abs(i - j)
        if gap < min_gap:
            min_gap = gap
            closest_note = j
    note_v = 0
    for j in listdir("installed-samples"):
        note = int(j[0:3])
        filepath = "installed-samples/"+j
        new_note = i
        semitone = new_note - note
        speed = 1 + (2 ** (semitone / 12) - 1)
        str_note = "%03d" % new_note
        other_than_note = j[3:]
        if note == closest_note:
            new_filepath = "installed-samples/" + str_note + other_than_note
            sox_cmd = "sox " + filepath + " " + new_filepath + " speed " + str(speed) + " rate -v"
            system(sox_cmd)
    print("Interpolating: " + str(i) + "... " + str(int(fc/len(empty_notes)*100)) + "%")
    fc = fc + 1

# Finalizing and Optimizing
fc = 0
for i in listdir("installed-samples"):
    filepath = "installed-samples/" + i
    tmp_filepath = "installed-samples/" + "tmp" + i
    rename(filepath, tmp_filepath)
    sox_cmd = "sox " + tmp_filepath  + " " + filepath + " gain -n -1.0"
    system(sox_cmd)
    remove(tmp_filepath)
    print("Finalizing and Optimizing: " + str(i) + "... " + str(int(fc/len(listdir("installed-samples"))*100)) + "%")
    fc = fc + 1

keepfile = open("installed-samples/.keep", "w")
keepfile.close()
print("Installation Completed. Enjoy!")

