/*
    Copyright © 2020 IceGuye.

    This program is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, version 3 of the
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include <Python.h>
#include <stdlib.h>

static PyObject *start_fadeout = NULL;
static PyObject *get_channel_busy = NULL;

static PyObject *start_fadeout_set_callback(PyObject *dummy, PyObject *args){
  PyObject *result = NULL;
  PyObject *temp;
  if (PyArg_ParseTuple(args, "O:set_callback", &temp)) {
    if (!PyCallable_Check(temp)) {
      PyErr_SetString(PyExc_TypeError, "parameter must be callable");
      return NULL;
    }
    Py_XINCREF(temp);
    Py_XDECREF(start_fadeout);
    start_fadeout = temp;
    Py_XINCREF(Py_None);
    result = Py_None;
  }
  return result;
}

static PyObject *get_channel_busy_set_callback(PyObject *dummy, PyObject *args){
  PyObject *result = NULL;
  PyObject *temp;
  if (PyArg_ParseTuple(args, "O:set_callback", &temp)) {
    if (!PyCallable_Check(temp)) {
      PyErr_SetString(PyExc_TypeError, "parameter must be callable");
      return NULL;
    }
    Py_XINCREF(temp);
    Py_XDECREF(get_channel_busy);
    get_channel_busy = temp;
    Py_XINCREF(Py_None);
    result = Py_None;
  }
  return result;
}

static PyObject *shrinking_playing_idx(PyObject *self, PyObject *args){
  PyObject *playing_idx;
  Py_ssize_t playing_idx_len;
  PyObject *playing_item;
  PyObject *channel_obj;
  PyObject *is_busy;
  int is_busy_num;
  PyObject *new_playing_idx;
  PyObject *arglist;

  if (!PyArg_ParseTuple(args, "O", &playing_idx)) {
    return NULL;
  }

  new_playing_idx = PyList_New(0);
  playing_idx_len = PyList_Size(playing_idx);
  Py_ssize_t i;
  for (i=0; i<playing_idx_len; i=i+1) {
    playing_item = PyList_GetItem(playing_idx, i);
    channel_obj = PyList_GetItem(playing_item, 0);
    arglist = Py_BuildValue("(O)", channel_obj);
    is_busy = PyObject_CallObject(get_channel_busy, arglist);
    is_busy_num = PyObject_IsTrue(is_busy);
    if (is_busy_num == 1) {
      PyList_Append(new_playing_idx, playing_item);
    }
    Py_DECREF(arglist);
    Py_DECREF(is_busy);
  }
  return new_playing_idx;
}

static PyObject *keypress_playing_idx(PyObject *self, PyObject *args){
  PyObject *playing_idx;
  Py_ssize_t playing_idx_len;
  PyObject *playing_item;
  PyObject *note_obj;
  long note;
  PyObject *keypress;
  PyObject *mid_pedal_hold;
  PyObject *key_vel_obj;
  long key_vel;
  PyObject *key_hold_obj;
  long key_hold;
  PyObject *new_playing_idx;

  if (!PyArg_ParseTuple(args, "OOO",
                        &playing_idx,
                        &keypress,
                        &mid_pedal_hold)) {
    return NULL;
  }

  new_playing_idx = PyList_New(0);
  playing_idx_len = PyList_Size(playing_idx);
  Py_ssize_t i;
  for (i=0; i<playing_idx_len; i=i+1) {
    playing_item = PyList_GetItem(playing_idx, i);
    note_obj = PyList_GetItem(playing_item, 1);
    note = PyLong_AsLong(note_obj);
    key_vel_obj = PyList_GetItem(keypress, note);
    key_vel = PyLong_AsLong(key_vel_obj);
    key_hold_obj = PyList_GetItem(mid_pedal_hold, note);
    key_hold = PyLong_AsLong(key_hold_obj);
    if (key_vel != 0 || key_hold != 0) {
      PyList_Append(new_playing_idx, playing_item);
    }
  }
  return new_playing_idx;
}

static PyObject *stoppers_down(PyObject *self, PyObject *args){
  PyObject *playing_idx;
  Py_ssize_t playing_idx_len;
  PyObject *keypress;
  PyObject *mid_pedal_hold;
  PyObject *holding_key_obj;
  long holding_key;
  PyObject *item_obj;
  PyObject *note_obj;
  PyObject *keypress_vel_obj;
  long note;
  long keypress_vel;
  PyObject *result;
  PyObject *arglist;
  
  if (!PyArg_ParseTuple(args, "OOO",
                        &playing_idx,
                        &keypress,
                        &mid_pedal_hold)) {
    return NULL;
  }
  playing_idx_len = PyList_Size(playing_idx);
  Py_ssize_t i;
  for (i=0; i<playing_idx_len; i=i+1) {
    item_obj = PyList_GetItem(playing_idx, i);
    note_obj = PyList_GetItem(item_obj, 1);
    note = PyLong_AsLong(note_obj);
    keypress_vel_obj = PyList_GetItem(keypress, note);
    keypress_vel = PyLong_AsLong(keypress_vel_obj);
    holding_key_obj = PyList_GetItem(mid_pedal_hold, note);
    holding_key = PyLong_AsLong(holding_key_obj);
    if (keypress_vel == 0 && holding_key == 0) {
      arglist = Py_BuildValue("(O)", item_obj);
      result = PyObject_CallObject(start_fadeout, arglist);
      Py_DECREF(arglist);
      Py_DECREF(result);
    }
  }
  Py_XINCREF(Py_None);
  return Py_None;
}

static PyMethodDef PlayinglistMethods[] = {
    {"start_fadeout_set_callback",
     start_fadeout_set_callback,
     METH_VARARGS,
     "Set the start_fadeout function to be able to callback in C."},
    {"get_channel_busy_set_callback",
     get_channel_busy_set_callback,
     METH_VARARGS,
     "Set the start_fadeout function to be able to callback in C."},
    {"shrinking_playing_idx",
     shrinking_playing_idx,
     METH_VARARGS,
     "Function to shrink playing_idx down in C."},
    {"keypress_playing_idx",
     keypress_playing_idx,
     METH_VARARGS,
     "Function to shrink playing_idx down based on keys pressed in C."},
    {"stoppers_down",
     stoppers_down,
     METH_VARARGS,
     "Function to simulate multiple stoppers down in C."},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef playinglistmodule = {
    PyModuleDef_HEAD_INIT,
    "playinglist",
    "High performance C functions for playinglist",
    -1,
    PlayinglistMethods
};

PyMODINIT_FUNC PyInit_playinglist(void) {
  return PyModule_Create(&playinglistmodule);
}
