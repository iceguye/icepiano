/*
    Copyright © 2020 IceGuye.

    This program is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation, version 3 of the
    License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include <Python.h>
#include <stdlib.h>
#include <math.h>

static PyObject *get_layer(PyObject *self, PyObject *args) {
  long note;
  double express;
  double volume;
  PyObject *notes;
  PyObject *layers;
  Py_ssize_t layers_len;
  PyObject *layer;
  Py_ssize_t selected_layer;
  double dbfs;
  double mindb;
  double diff_dbfs;
  PyObject *layer_dbfs_obj;
  double layer_dbfs;

  if (!PyArg_ParseTuple(args, "lddO",
                        &note,
                        &express,
                        &volume,
                        &notes)) {
    return NULL;
  }

  if (express == 0.0 || volume == 0.0) {
    dbfs = -999.99;
  } else {
    dbfs = 40 * log((volume*express)/(127*127));
  }
  mindb = 99999.99;
  layers = PyList_GetItem(notes, note);
  layers_len = PyList_Size(layers);
  selected_layer = 0;
  Py_ssize_t i;
  for (i=0; i<layers_len; i=i+1){
    layer = PyList_GetItem(layers, i);
    layer_dbfs_obj = PyList_GetItem(layer, 1);
    layer_dbfs = PyFloat_AsDouble(layer_dbfs_obj);
    diff_dbfs = fabs(dbfs - layer_dbfs);
    if (diff_dbfs < mindb) {
      mindb = diff_dbfs;
      selected_layer = i;
    }
  }
  
  return PyLong_FromLong(selected_layer);
}

static PyMethodDef PreparesoundMethods[] = {
  {"get_layer",
   get_layer,
   METH_VARARGS,
   "Function to find which layer of sound should play."
  },
  {NULL, NULL, 0, NULL}
};

static struct PyModuleDef preparesoundmodule = {
  PyModuleDef_HEAD_INIT,
  "preparesound",
  "High performance C functions to prepare final sound.",
  -1,
  PreparesoundMethods
};

PyMODINIT_FUNC PyInit_preparesound(void) {
  return PyModule_Create(&preparesoundmodule);
}
