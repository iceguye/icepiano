#!/usr/bin/python3

#    Copyright © 2020 - 2021 IceGuye, Hao

#     This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published
#    by the Free Software Foundation, version 3 of the License.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#    General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see
#    <http://www.gnu.org/licenses/>.

import os
import sys
import shutil
import math
from scipy.io import wavfile
import scipy.fft
import numpy as np
from multiprocessing import Process
from pydub import AudioSegment
# import matplotlib.pyplot as plt

def sox_change_pitch(filepath, chcent):
    filename_start = filepath.rfind("/") + 1
    if filename_start == 0:
        filename = filepath
        folder_path = ""
    else:
        filename = filepath[filename_start:]
        folder_path = filepath[:filename_start]
    tmp_path = folder_path + "tmp-" + filename
    os.rename(filepath, tmp_path)
    os.system("sox " + tmp_path + " " + filepath + " pitch " + str(chcent) + " rate -v")
    os.remove(tmp_path)

def sox_change_pitch_spd(filepath, chcent):
    filename_start = filepath.rfind("/") + 1
    if filename_start == 0:
        filename = filepath
        folder_path = ""
    else:
        filename = filepath[filename_start:]
        folder_path = filepath[:filename_start]
    tmp_path = folder_path + "tmp-" + filename
    os.rename(filepath, tmp_path)
    speed = 1 + (2 ** ((chcent / 100) / 12) - 1)
    os.system("sox " + tmp_path + " " + filepath + " speed " + str(speed) + " rate -v")
    os.remove(tmp_path)

def check_base_chcent(base_freq, hs, fourier_0, fourier_1, fac_0, fac_1):
    if base_freq * 0.25 < (base_freq * hs) * 0.03:
        freq_start = base_freq * hs - base_freq * 0.25
        freq_end = base_freq * hs + base_freq * 0.25
    else:
        freq_start = base_freq * hs * (1 - 0.03)
        freq_end = base_freq * hs * (1 + 0.03)
    vol_max_0 = -999
    vol_max_1 = -999
    max_ind_0 = int(base_freq * hs * fac_0)
    max_ind_1 = int(base_freq *hs * fac_1)

    win_start = int(freq_start * fac_0)
    win_end = int(freq_end * fac_0)
    i = win_start
    while(i <= win_end):
        if abs(fourier_0[i]) > vol_max_0:
            vol_max_0 = abs(fourier_0[i])
            max_ind_0 = i
        i = i + 1

    win_start = int(freq_start * fac_1)
    win_end = int(freq_end * fac_1)
    i = win_start
    while(i <= win_end):
        if abs(fourier_1[i]) > vol_max_1:
            vol_max_1 = abs(fourier_1[i])
            max_ind_1 = i
        i = i + 1
    real_freq_0 = max_ind_0 / fac_0
    real_freq_1 = max_ind_1 / fac_1
    chsemi_0 = 12 * math.log(base_freq * hs / real_freq_0) / math.log(2)
    chsemi_1 = 12 * math.log(base_freq * hs / real_freq_1) / math.log(2)
    chcent_0 = chsemi_0 * 100
    chcent_1 = chsemi_1 * 100
    chcent = [chcent_0, chcent_1]
    return (chcent)

def rename_samples(filename):
    ext_pos = filename.find(".wav")
    no_ext_name = filename[:ext_pos]
    vpos = no_ext_name.find("v")
    if vpos != -1:
        note = no_ext_name[:vpos]
        a0 = 21
        start_point = 0
        if "A" in note:
            start_point = a0
        elif "A#" in note or "Bb" in note:
            start_point = a0 + 1
        elif "B" in note:
            start_point = a0 + 2
        elif "G#" in note or "Ab" in note:
            start_point = a0 - 1
        elif "G" in note:
            start_point = a0 - 2
        elif "F#" in note or "Ab" in note:
            start_point = a0 - 3
        elif "F" in note:
            start_point = a0 - 4
        elif "E" in note:
            start_point = a0 - 5
        elif "D#" in note or "Eb" in note:
            start_point = a0 - 6
        elif "D" in note:
            start_point = a0 - 7
        elif "C#" in note or "Db" in note:
            start_point = a0 - 8
        elif "C" in note:
            start_point = a0 - 9
            
        octave = int(note[len(note)-1:])
        midi_num = start_point + (octave * 12)
        str_midi_num = "%03d" % midi_num
        new_filename = str_midi_num + "-" + filename

        src = "samples/" + filename
        dst = "perfecths-samples/" + new_filename
        perfect_filename = "perfect-" + new_filename
        perfect_dst = "perfecths-samples/" + perfect_filename

        if os.path.isfile(dst) == False and os.path.isfile(perfect_dst) == False:
            print(src + " ====> " + dst)
            sox_cmd = "sox " + src + " -b 32 -e floating-point " + dst + " gain -1.0"
            os.system(sox_cmd)

def start_perfection (arg_ls, filename):
    ahz = float(arg_ls[1])
    hz_factor = ahz / ((2 ** (1/12)) ** 69)
    running = True
    if "keep" in filename or "perfect" in filename or os.path.isfile("perfecths-samples/perfect-"+filename):
        running = False
    if running == True:
        print("Perfecting: " + filename)
        midi_note = int(filename[:3])
        base_freq = hz_factor * ((2 ** (1/12)) ** midi_note)
        filepath = "perfecths-samples/" + filename
        fps, sound = wavfile.read(filepath)
        # os.remove(filepath)
        sound_0 = sound[:, 0]
        sound_1 = sound[:, 1]
        test_s_0 = sound_0.copy()
        test_s_1 = sound_1.copy()
        test_s_0 = test_s_0[int(0.3 * fps):]
        test_s_1 = test_s_1[int(0.3 * fps):]
        test_f_0 = scipy.fft.rfft(test_s_0)
        test_f_1 = scipy.fft.rfft(test_s_1)
        test_fc_0 = len(test_f_0) / (fps / 2)
        test_fc_1 = len(test_f_1) / (fps / 2)
        # plt.plot(np.fft.fftfreq(len(fourier_0)), fourier_0)
        # plt.show()
        # exit()
        chcent = check_base_chcent(base_freq,
                                  1,
                                  test_f_0,
                                  test_f_1,
                                  test_fc_0,
                                  test_fc_1)
        chcent_0 = chcent[0]
        chcent_1 = chcent[1]
        # print(chcent_0)
        # print(chcent_1)
        # print("======================================")
        
        filepath_0 = "perfecths-samples/0-" + filename
        filepath_1 = "perfecths-samples/1-" + filename
        wavfile.write(filepath_0, fps, sound_0.astype(np.float32))
        wavfile.write(filepath_1, fps, sound_1.astype(np.float32))

        sox_change_pitch_spd(filepath_0, chcent_0)
        sox_change_pitch_spd(filepath_1, chcent_1)
        
        fps_0, sound_0 = wavfile.read(filepath_0)
        fps_1, sound_1 = wavfile.read(filepath_1)
        os.remove(filepath_0)
        os.remove(filepath_1)

        test_s_0 = sound_0.copy()
        test_s_1 = sound_1.copy()
        test_s_0 = test_s_0[int(0.3 * fps_0):]
        test_s_1 = test_s_1[int(0.3 * fps_1):]
        test_f_0 = scipy.fft.rfft(test_s_0)
        test_f_1 = scipy.fft.rfft(test_s_1)
        test_fc_0 = len(test_f_0) / (fps_0 / 2)
        test_fc_1 = len(test_f_1) / (fps_1 / 2)

        sound_0_bw = (sound_0.copy() * -0)[::-1]
        sound_1_bw = (sound_1.copy() * -0)[::-1]
        sound_0 = np.concatenate((sound_0, sound_0_bw))
        sound_1 = np.concatenate((sound_1, sound_1_bw))
        fourier_0 = scipy.fft.rfft(sound_0)
        fac_0 = len(fourier_0) / (fps_0 / 2)
        fourier_1 = scipy.fft.rfft(sound_1)
        fac_1 = len(fourier_1) / (fps_1 / 2)
        
        hz = hz_factor * ((2 ** (1/12)) ** midi_note)
        hs_n = math.floor(18000 / hz)
        if hs_n > 20:
            hs_n = 20
        non_win_rfft_0 = np.copy(fourier_0)
        non_win_rfft_1 = np.copy(fourier_1)
        i = 2
        while(i <= hs_n):
            hshz = hz * i
            if hz * 0.25 < hshz * 0.03:
                freq_start = hshz - hz * 0.25
                freq_end = hshz + hz * 0.25
            else:
                freq_start = hshz * (1 - 0.03)
                freq_end = hshz * (1 + 0.03)

            win_max_0 = -999
            win_max_1 = -999
            win_freq_0 = hshz
            win_freq_1 = hshz
            
            win_start = int(freq_start * test_fc_0)
            win_end = int(freq_end * test_fc_0)
            j = win_start
            while(j <= win_end):
                if abs(test_f_0[j]) > win_max_0:
                    win_max_0 = abs(test_f_0[j])
                    win_freq_0 = j / test_fc_0
                j = j + 1
                
            win_start = int(freq_start * test_fc_1)
            win_end = int(freq_end * test_fc_1)
            j = win_start
            while(j <= win_end):
                if abs(test_f_1[j]) > win_max_1:
                    win_max_1 = abs(test_f_1[j])
                    win_freq_1 = j / test_fc_1
                j = j + 1
                
            j = 1
            hz_note_down = 0
            hz_note = 0
            while (hz_note <= hshz):
                hz_note_down = hz_note
                hz_note = hz_factor * ((2 ** (1/12)) ** (midi_note + j))
                j = j + 1
            delta_down = abs(hz_note_down - hshz)
            delta_up = abs(hz_note - hshz)
            if delta_down > delta_up:
                target_freq = hz_note
            else:
                target_freq = hz_note_down
            chhz = target_freq / win_freq_0
            chocta_0 = math.log(chhz) / math.log(2)
            chsemi_0 = 12 * chocta_0
            chcent_0 = chsemi_0 * 100

            j = 1
            hz_note_down = 0
            hz_note = 0
            while (hz_note <= hshz):
                hz_note_down = hz_note
                hz_note = hz_factor * ((2 ** (1/12)) ** (midi_note + j))
                j = j + 1
            delta_down = abs(hz_note_down - hshz)
            delta_up = abs(hz_note - hshz)
            if delta_down > delta_up:
                target_freq = hz_note
            else:
                target_freq = hz_note_down
            chhz = target_freq / win_freq_1
            chocta_1 = math.log(chhz) / math.log(2)
            chsemi_1 = 12 * chocta_1
            chcent_1 = chsemi_1 * 100

            # print(chcent_0)
            # print(chcent_1)
            # print("----------------------------------------")

            win_rfft_0 = np.copy(fourier_0)
            win_rfft_1 = np.copy(fourier_1)
            win_zeros_0 = np.zeros(len(win_rfft_0), dtype=complex)
            win_zeros_1 = np.zeros(len(win_rfft_1), dtype=complex)

            freq_start = hshz - hz * 0.5
            freq_end = hshz + hz * 0.5
            
            win_start = int(freq_start * fac_0)
            win_end = int(freq_end * fac_0) - 1 
            j = win_start
            while(j <= win_end):
                non_win_rfft_0[j] = 0
                win_zeros_0[j] = win_rfft_0[j]
                j = j + 1
            win_rfft_0 = win_zeros_0

            win_start = int(freq_start * fac_1)
            win_end = int(freq_end * fac_1) 
            j = win_start
            while(j <= win_end):
                non_win_rfft_1[j] = 0
                win_zeros_1[j] = win_rfft_1[j]
                j = j + 1
            win_rfft_1 = win_zeros_1
            
            win_sound_0 = scipy.fft.irfft(win_rfft_0)
            win_sound_1 = scipy.fft.irfft(win_rfft_1)
            win_sound_0 = win_sound_0.transpose()
            win_sound_1 = win_sound_1.transpose()
            
            win_path_0 = "perfecths-samples/win-0-" + filename
            win_path_1 = "perfecths-samples/win-1-" + filename

            wavfile.write(win_path_0, fps_0, win_sound_0.astype(np.float32))
            wavfile.write(win_path_1, fps_1, win_sound_1.astype(np.float32))

            sox_change_pitch_spd(win_path_0, chcent_0)
            sox_change_pitch_spd(win_path_1, chcent_1)

            fps_0, win_sound_0 = wavfile.read(win_path_0)
            fps_1, win_sound_1 = wavfile.read(win_path_1)
            win_sound_0 = np.array_split(win_sound_0, 2)
            win_sound_0 = win_sound_0[0]
            win_sound_1 = np.array_split(win_sound_1, 2)
            win_sound_1 = win_sound_1[0]
            os.remove(win_path_0)
            os.remove(win_path_1)
            
            non_win_sound_0 = scipy.fft.irfft(non_win_rfft_0)
            non_win_sound_1 = scipy.fft.irfft(non_win_rfft_1)
            non_win_sound_0 = np.array_split(non_win_sound_0, 2)
            non_win_sound_0 = non_win_sound_0[0]
            non_win_sound_1 = np.array_split(non_win_sound_1, 2)
            non_win_sound_1 = non_win_sound_1[0]
            if len(non_win_sound_0) < len(win_sound_0):
                tmp_sound_0 = win_sound_0.copy()
                tmp_sound_0[:len(non_win_sound_0)] += non_win_sound_0
            else:
                tmp_sound_0 = non_win_sound_0.copy()
                tmp_sound_0[:len(win_sound_0)] += win_sound_0
            non_win_sound_0 = tmp_sound_0
            if len(non_win_sound_1) < len(win_sound_1):
                tmp_sound_1 = win_sound_1.copy()
                tmp_sound_1[:len(non_win_sound_1)] += non_win_sound_1
            else:
                tmp_sound_1 = non_win_sound_1.copy()
                tmp_sound_1[:len(win_sound_1)] += win_sound_1
            non_win_sound_1 = tmp_sound_1
            non_win_sound_0_bw = (non_win_sound_0.copy() * -1)[::-1]
            non_win_sound_1_bw = (non_win_sound_1.copy() * -1)[::-1]
            non_win_sound_0 = np.concatenate((non_win_sound_0, non_win_sound_0_bw))
            non_win_sound_1 = np.concatenate((non_win_sound_1, non_win_sound_1_bw))
            non_win_rfft_0 = scipy.fft.rfft(non_win_sound_0)
            non_win_rfft_1 = scipy.fft.rfft(non_win_sound_1)
            i = i + 1
            
        non_win_sound_0 = scipy.fft.irfft(non_win_rfft_0)
        non_win_sound_1 = scipy.fft.irfft(non_win_rfft_1)
        if len(non_win_sound_0) < len(non_win_sound_1):
            tmp_zeros = np.zeros(len(non_win_sound_1), dtype=complex)
            i = 0
            while (i < len(non_win_sound_0)):
                tmp_zeros[i] = non_win_sound_0[i]
                i = i + 1
            non_win_sound_0 = tmp_zeros
        elif len(non_win_sound_0) > len(non_win_sound_1):
            tmp_zeros = np.zeros(len(non_win_sound_0), dtype=complex)
            i = 0
            while (i < len(non_win_sound_1)):
                tmp_zeros[i] = non_win_sound_1[i]
                i = i + 1
            non_win_sound_1 = tmp_zeros
            
        non_win_sound_0 = np.array_split(non_win_sound_0, 2)
        non_win_sound_0 = non_win_sound_0[0]
        non_win_sound_1 = np.array_split(non_win_sound_1, 2)
        non_win_sound_1 = non_win_sound_1[0]
        non_win_sound = np.stack((non_win_sound_0, non_win_sound_1))
        non_win_sound = non_win_sound.transpose()
        non_win_sound = non_win_sound.real
        non_win_path = "perfecths-samples/non-win-" + filename
        wavfile.write(non_win_path, fps, non_win_sound.astype(np.float32))
        perfect_path = "perfecths-samples/perfect-" + filename
        os.rename(non_win_path, perfect_path)

arg_ls = sys.argv
if (len(arg_ls) != 3):
    error =''' 

    You must have 2 arguments for A4 frequency and the number threads
    you want.

    Example:

        python perfecths.py 440 16

    '''
    print(error)
    sys.exit()

for filename in os.listdir("perfecths-samples"):
    if "win"  in filename or "tmp" in filename:
        print("Removing " + filename)
        os.remove("perfecths-samples/" + filename)

process_num = int(arg_ls[2])

process_list = [None] * process_num
file_list = os.listdir("samples/").copy()
i = 0
while (i<len(file_list)):
    filename = file_list[i]
    has_been_assigned = False
    while (has_been_assigned == False):
        j = 0
        p = process_list
        while(j<len(p)):
            if p[j] is None:
                p[j] = Process(target=rename_samples, args=(filename,))
                p[j].start()
                has_been_assigned = True
                break
            elif p[j].is_alive() == False:
                p[j].close()
                p[j] = Process(target=rename_samples, args=(filename,))
                p[j].start()
                has_been_assigned = True
                break
            j = j + 1
    i = i + 1

i = 0
while(i < len(process_list)):
    if process_list[i] != None:
        process_list[i].join()
    i = i + 1

process_list = [None] * process_num
file_list = os.listdir("perfecths-samples").copy()
i = 0
while (i<len(file_list)):
    filename = file_list[i]
    has_been_assigned = False
    while (has_been_assigned == False):
        j = 0
        p = process_list
        while(j<len(p)):
            if p[j] is None:
                p[j] = Process(target=start_perfection, args=(arg_ls, filename))
                p[j].start()
                has_been_assigned = True
                break
            elif p[j].is_alive() == False:
                p[j].close()
                p[j] = Process(target=start_perfection, args=(arg_ls, filename))
                p[j].start()
                has_been_assigned = True
                break
            j = j + 1
    i = i + 1
i = 0
while(i < len(process_list)):
    if process_list[i] != None:
        process_list[i].join()
    i = i + 1

# rename_samples("C3v4.wav")
# start_perfection(arg_ls, "048-C3v4.wav")

for filename in os.listdir("perfecths-samples"):
    if "perfect" not in filename and "keep" not in filename:
        os.remove("perfecths-samples/"+filename)
